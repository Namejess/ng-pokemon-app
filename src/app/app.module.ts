import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BrowserModule } from "@angular/platform-browser";
import { ClarityModule } from "@clr/angular";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PageNotFoundComponent } from "./page-not-found/page-not-found.component"; //fait avec CLi : ng g directive border-card
import { PokemonModule } from "./pokemon/pokemon.module";
import { DataServiceComponent } from './services/data-service/data-service.component';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from "ngx-pagination";

@NgModule({
  declarations: [AppComponent, PageNotFoundComponent],
  imports: [BrowserModule, FormsModule, PokemonModule, AppRoutingModule, ClarityModule, HttpClientModule, NgxPaginationModule],
  providers: [DataServiceComponent, HttpClientModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
