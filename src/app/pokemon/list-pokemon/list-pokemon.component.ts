import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { PokemonService } from "../pokemon.service";
import { DataServiceComponent } from "../../services/data-service/data-service.component";
import { NgxPaginationModule } from "ngx-pagination/public-api";

@Component({
  selector: "app-list-pokemon",
  templateUrl: "./list-pokemon.component.html",
  styleUrls: ["./list-pokemon.component.css"],
})
export class ListPokemonComponent implements OnInit {
  // pokemonList: Pokemon[];
  pokemons: any[] = [];
  page = 0;
  numberPokemon = 8;
  totalPokemons: number;

  constructor(

    private dataService: DataServiceComponent
  ) {}

  ngOnInit(): void {
    this.getPokemons();
  }
  getPokemons() {
    this.dataService
      .getPokemons(this.numberPokemon, this.page + 0)
      .subscribe((response: any) => {
        this.totalPokemons = response.count;
        
        response.results.forEach((result: any) => {
          this.dataService
            .getMorePokemons(result.name)
            .subscribe((uniqResponse: any) => {
              console.log(this.pokemons);
              this.pokemons.push(uniqResponse);
            });
        });
      });
  }
  // goToPokemon(pokemon: Pokemon) {
  //   this.router.navigate(["/pokemons", pokemon.id]);
  // }
}
